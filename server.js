const { query } = require('express');
const express = require('express');
const app = express();
const mysql = require('mysql');
const bodyParser = require("body-parser");

const port = 3000;

app.listen(port, () => {
    console.log("Serverul ruleaza pe portul " + port);
})

app.use('/', express.static("./front-end"));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "lista"
});

connection.connect( (err) => {
    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS obiective(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, lucru VARCHAR(255))";
    connection.query(sql, (err, result) => {
        if(err) throw err;
    });
});

app.post('/', (req, res) => {
    const obiect = {
        lucru: req.body.lucru
    };
    const sql = "INSERT INTO obiective(lucru) VALUES (?)";
    connection.query(sql, obiect.lucru, (err, result) => {
        if(err){
            res.status(500).send(error);
        }else
        {
            obiect.id = result.insertId;
            console.log("Lucru adaugat cu succes!");
            res.status(200).send(obiect);
        }
    })
})

app.get('/info', (req, res) => {
    const sql = "SELECT * FROM obiective";
    connection.query(sql, (err, result) => {
        if(err) throw err;
        result=JSON.parse(JSON.stringify(result))
        console.log(result);
        res.status(200).json(result);
    })
})